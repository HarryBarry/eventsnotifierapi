import models from '../models';

export const getAllOrders = (req, res) => {
  models.order.findAll()
  .then((order)=>{
    res.json({response: order});
  }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};

export const getOrderById = (req, res) => {
  models.order.findOne(
    { where: { id: req.params.order_id }}
  ).then((order)=>{
    res.json({response: order});
  }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};

export const getOrdersByContractorId = (req, res) => {
  models.order.findAll(
    { where: { contractor_id: req.params.contractor_id }}
  ).then((order)=>{
    res.json({response: order});
  }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};

export const createOrder = (req, res, next) => {
  models.order.create(
    {
      delivery_date_from: req.body.delivery_date_from,
      delivery_date_to: req.body.delivery_date_to,
      number_of_storeys: req.body.number_of_storeys,
      filling: req.body.filling,
      decor: req.body.decor,
      event_id: req.body.event_id,
      description: req.body.description,
      contractor_id: req.params.contractor_id
    }
  ).then((order)=>{
    res.json({response: order});
  }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};

export const updateOrder = (req, res, next) => {
  models.order.update(
    {
      delivery_date_from: req.body.delivery_date_from,
      delivery_date_to: req.body.delivery_date_to,
      number_of_storeys: req.body.number_of_storeys,
      filling: req.body.filling,
      decor: req.body.decor,
      event_id: req.body.event_id,
      description: req.body.description,
      contractor_id: req.params.contractor_id
    },
    { where: { id: req.params.order_id } }
  ).then((order)=>{
    models.order.findOne(
      { where: { id: req.params.order_id } }
    ).then((order)=>{
      let clientMessage = (order)?
        `Successfully updated order with id: ${req.params.order_id}`:
        `Not found order with id: ${req.params.order_id}`;
      res.json({response: {
        order: order,
        clientMessage: clientMessage
      } });
    }).catch((err)=>{
      res.json({response: { serverError: err }});
    });
  }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};

export const deleteOrder = (req, res) => {
  models.order.destroy(
  { where: { id: req.params.order_id } }
  ).then((order)=>{
    let clientMessage = (order)?
      `Successfully deleted order with id: ${req.params.order_id}`:
      `Not found order with id: ${req.params.order_id}`;
    res.json({response: {clientMessage: clientMessage} });
  }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};
