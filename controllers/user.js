import jwt from 'jsonwebtoken';
import config from '../config';
import models from '../models';

export async function getCurrentUser(req, res, next) {
  let tokenDirty = req.headers['authorization'];

  if (tokenDirty) {
    let token = tokenDirty.replace('Bearer ', '');

    jwt.verify(token, config.secret, (err, decoded)=>{
      if (err)
        return res.status(401).json({response: { clientMessage: 'Invalid token!' } });

      return models.user.findOne({
        where: {
          id: decoded.id
        }
      }).then((user) => {
        if (user) res.json({ response: user });
      });
    });
  } else {
    return res.status(401).json({response: { clientMessage: 'Not found token!'} });
  }
}