import models from '../models';

export const getAllContractors = (req, res, next) => {
    models.contractor.findAll({
      include: [models.event, models.order]
    }).then((contractor)=>{
      res.json({response: contractor});
    }).catch((err)=>{
      res.json({response: { serverError: err }});
    });
};

export const getContractorById = (req, res, next) => {
    models.contractor.findOne({
      where: { id: req.params.contractor_id }
    },{
      include: [models.event, models.order]
    }).then((contractor)=>{
      res.json({response: contractor});
    }).catch((err)=>{
      res.json({response: { serverError: err }});
    });
};

export const createContractor = (req, res, next) => {
  let contractor = {};
  if (req.body.status) contractor.status = req.body.status;

  models.contractor.create(contractor).then((contractor)=>{
    res.json({response: contractor});
  }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};

export const updateContractor = (req, res, next) => {
  models.contractor.update(
  { status: req.body.status },
  { where: { id: req.params.contractor_id } }
  ).then((contractor)=>{
    models.contractor.findOne(
      { where: { id: req.params.contractor_id } }
    ).then((contractor)=>{
      let clientMessage = (contractor)?
        `Successfully updated contractor with id: ${req.params.contractor_id}`:
        `Not found contractor with id: ${req.params.contractor_id}`;
      res.json({response: {
        contractor: contractor,
        clientMessage: clientMessage
      } });
    }).catch((err)=>{
      res.json({response: { serverError: err }});
    });
  }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};

export const deleteContractor = (req, res, next) => {
  models.contractor.destroy({
    where: {
      id: req.params.contractor_id
    }
  }).then((data)=>{
    let clientMessage = (data)?
      `Successfully deleted contractor with id: ${req.params.contractor_id}`:
      `Not found contractor with id: ${req.params.contractor_id}`;
    res.json({response: {clientMessage: clientMessage} });
  }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};
