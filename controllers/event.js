import models from '../models';

export const getAllEvents = (req, res) => {
  models.event.findAll({
    include: [models.order]
  })
    .then((event)=>{
      res.json({response: event});
    }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};

export const getEventById = (req, res) => {
  models.event.findOne({
    where: { id: req.params.event_id }
  },{
    include: [models.order]
  })
    .then((event)=>{
      res.json({response: event});
    }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};

export const getEventsByContractorId = (req, res) => {
  models.event.findAll({
    where: { contractor_id: req.params.contractor_id }
  },{
    include: [models.order]
  })
    .then((event)=>{
      res.json({response: event});
    }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};

export const createEvent = (req, res, next) => {
  models.event.create({
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    middle_name: req.body.middle_name,
    interval: req.body.interval,
    event_type: req.body.event_type,
    description: req.body.description,
    contractor_id: req.params.contractor_id
  }).then((event)=>{
    res.json({response: event});
  }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};

export const updateEvent = (req, res, next) => {
  models.event.update(
    {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      middle_name: req.body.middle_name,
      interval: req.body.interval,
      event_type: req.body.event_type,
      description: req.body.description,
      contractor_id: req.params.contractor_id
    },
    { where: { id: req.params.event_id } }
  ).then((event)=>{
    models.event.findOne(
      { where: { id: req.params.event_id } }
    ).then((event)=>{
      let clientMessage = (event)?
        `Successfully updated event with id: ${req.params.event_id}`:
        `Not found event with id: ${req.params.event_id}`;
      res.json({response: {
        event: event,
        clientMessage: clientMessage
      } });
    }).catch((err)=>{
      res.json({response: { serverError: err }});
    });
  }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};

export const deleteEvent = (req, res) => {
  models.event.destroy({
    where: {
        id: req.params.event_id
    }
  }).then((event)=>{
    let clientMessage = (event)?
      `Successfully deleted event with id: ${req.params.event_id}`:
      `Not found event with id: ${req.params.event_id}`;

    res.json({response: {clientMessage: clientMessage} });
  }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};
