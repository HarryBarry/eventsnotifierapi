import models from '../models';

export const signup = (req, res, next) => {
  models.user.createNewUser({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password
  }).then((user)=>{
    res.json({response: user});
  }).catch((err)=>{
    res.json({response: { serverError: err }});
  });

};

export const signin = (req, res, next) => {
  models.user.authorization({
    email: req.body.email,
    password: req.body.password
  }).then((user)=>{
    res.json({response: user});
  }).catch((err)=>{
    res.json({response: { serverError: err }});
  });
};
