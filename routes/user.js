import { Router } from 'express';
import * as UserController from '../controllers/user';

export default () => {
  let router = Router();

  router.get('/getCurrentUser', UserController.getCurrentUser);

  return router;
}