import { Router } from 'express';
import * as AuthController from '../controllers/auth';

export default () => {
  let router = Router();

  router.post('/signup', AuthController.signup);
  router.post('/signin', AuthController.signin);

  return router;
}