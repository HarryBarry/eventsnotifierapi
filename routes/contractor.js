import { Router } from 'express';
import * as ContractorController from '../controllers/contractor';

export default () => {
  let router = Router();

  router.get('/contractors', ContractorController.getAllContractors);
  router.get('/contractors/:contractor_id', ContractorController.getContractorById);
  router.post('/contractors/create', ContractorController.createContractor);
  router.put('/contractors/:contractor_id/update', ContractorController.updateContractor);
  router.delete('/contractors/:contractor_id/delete', ContractorController.deleteContractor);
  
  return router;
};
