import { Router } from 'express';
import * as EventController from '../controllers/event';

export default () => {
  let router = Router();
  
  router.get('/events', EventController.getAllEvents);
  router.get('/events/:event_id', EventController.getEventById);
  router.get('/contractors/:contractor_id/events', EventController.getEventsByContractorId);
  router.post('/contractors/:contractor_id/events/create', EventController.createEvent);
  router.put('/contractors/:contractor_id/events/:event_id/update', EventController.updateEvent);
  router.delete('/contractors/:contractor_id/events/:event_id/delete', EventController.deleteEvent);

  return router;
}
