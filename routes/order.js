import { Router } from 'express';
import * as OrderController from '../controllers/order';

export default () => {
  let router = Router();

  router.get('/orders', OrderController.getAllOrders);
  router.get('/orders/:order_id', OrderController.getOrderById);
  router.get('/contractors/:contractor_id/orders', OrderController.getOrdersByContractorId);
  router.post('/contractors/:contractor_id/orders/create', OrderController.createOrder);
  router.put('/contractors/:contractor_id/orders/:order_id/update', OrderController.updateOrder);
  router.delete('/contractors/:contractor_id/orders/:order_id/delete', OrderController.deleteOrder);

  return router;
};
