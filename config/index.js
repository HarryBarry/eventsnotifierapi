const config = {
    mongoDB: {host:'mongodb://localhost', port: ':27017', name:'/EventsNotifier'},
    mysqlDB: {host:'localhost', port: 3306, user: 'root', pass: '', name:'EventsNotifier'},
    session : {secret: 'CFBEAA47A7165BB2B979E58F6DB83', name: 'session_id', saveUninitialized: true, resave: true},
    serverPort: 9999,
    secret: 'CFBEAA47A7165BB2B979E58F6DB83'
};

export default config;