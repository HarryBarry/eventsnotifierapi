import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import flash from 'express-flash';
import helmet from 'helmet';
import cors from 'cors';

// Configs
import config from './config';
import {API_URL} from './constants/app';


// Middlewares include
import {logErrors, clientErrorHandler, errorHandler} from './middlewares/errorHandler';
import checkToken from './middlewares/checkToken';

// Routes
import authRoutes from './routes/auth';
import userRoutes from './routes/user';
import contractorRoutes from './routes/contractor';
import eventRoutes from './routes/event';
import orderRoutes from './routes/order';


const app = express();

// Middleware
app.use(express.static(__dirname+'public'));
app.use(helmet());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(session(config.session));
app.use(flash());
app.use(logErrors);
app.use(clientErrorHandler);
app.use(errorHandler);

// 3rd party middleware
app.use(cors());

// Routes include
app.use(`${API_URL}`, authRoutes());
app.use(`${API_URL}`, userRoutes());
app.use(`${API_URL}`, checkToken, contractorRoutes()); //checkToken,
app.use(`${API_URL}`, checkToken, eventRoutes()); //checkToken,
app.use(`${API_URL}`, checkToken, orderRoutes()); //checkToken,

// Errors handle
app.use(function(req, res) {
    res.status(404);
    res.json({title:'Sorry page not found', status: 404});
});

app.use(function(err, req, res, next) {
    res.status(500);
    res.json({title:'Internal Server Error', status: 500, error: err});
});

module.exports = app;