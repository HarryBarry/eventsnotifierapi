module.exports = (sequelize, DataTypes) => {
    let order = sequelize.define('order', {
        delivery_date_from: {
            type: DataTypes.DATE
        },
        delivery_date_to: {
            type: DataTypes.DATE
        },
        number_of_storeys: {
            type: DataTypes.INTEGER
        },
        filling: {
            type: DataTypes.STRING
        },
        decor: {
            type: DataTypes.STRING
        },
        description: {
            type: DataTypes.TEXT
        }
    }, {
      underscore: true,
      classMethods: {
        associate: (models) => {
          order.belongsTo(models.contractor);
        }
      }
    });

    return order;
};