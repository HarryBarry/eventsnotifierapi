import bcrypt from 'bcrypt-nodejs';
import jwt from 'jsonwebtoken';
import config from '../config';

module.exports = (sequelize, DataTypes) => {
    let User = sequelize.define('user', {
        email: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        password: {
            type: DataTypes.STRING
        },
        status: {
          type: DataTypes.ENUM('new', 'confirmed', 'active', 'disabled'),
          defaultValue: 'new'
        },
        token: {
          type: DataTypes.STRING,
          set: function (newToken) {
            this.setDataValue('token', newToken);
            this.tokenCreatedAt = Date.now();
          }
        },
        tokenCreatedAt: {
          type: DataTypes.DATE
        }
    }, {
        instanceMethods: {
          toJSON: function () {
            var values = Object.assign({}, this.get());

            delete values.password;
            return values;
          },
          encryptPassword: (password, dbPass) => {
            return bcrypt.compareSync(password, dbPass);
          },
          createToken: (user) => {
            if (!user.token) {
              user.token = User.generateToken(user.id);
              user.save();
            }

            return user.token;
          }
        },
      classMethods: {
        createNewUser: (user) => {
          return User.create({
            name: user.name,
            email: user.email,
            password: bcrypt.hashSync(user.password, bcrypt.genSaltSync(8), null)
          });
        },
        generateToken: (id) => {
          return jwt.sign({ id: id }, config.secret);
        },
        authorization: ({email, password}) => {
          return User.findOne({
            where: {
              email: email
            }
          }).then((foundUser) => {
            if (!foundUser) {
              return { user: null, clientError: 'Invalid credentials' };
            } else if (foundUser.encryptPassword(password, foundUser.password)) {
              return {
                user: {
                  id: foundUser.id,
                  name: foundUser.name,
                  email: foundUser.email,
                  status: foundUser.status,
                  tokenCreatedAt: foundUser.tokenCreatedAt,
                  createdAt: foundUser.createdAt,
                  updatedAt: foundUser.updatedAt,
                  token: foundUser.createToken(foundUser)
                }
              };
            } else if (foundUser.encryptPassword(password, foundUser.password)) {
              return { user: null, clientError: 'Invalid credentials' };
            } else {
              return {
                user: {
                  id: foundUser.id,
                  name: foundUser.name,
                  email: foundUser.email,
                  status: foundUser.status,
                  tokenCreatedAt: foundUser.tokenCreatedAt,
                  createdAt: foundUser.createdAt,
                  updatedAt: foundUser.updatedAt,
                  token: foundUser.createToken(foundUser)
                }
              };
            }
          });
        }
      }
    });

  return User;
};