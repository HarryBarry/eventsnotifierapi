module.exports = (sequelize, DataTypes) => {
    let contractor = sequelize.define('contractor', {
        status: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
        }
    }, {
        underscored: true,
        classMethods: {
            associate: (models) => {
              contractor.hasMany(models.event);
              contractor.hasMany(models.order);
            }
        }
    });

    
    return contractor;
};