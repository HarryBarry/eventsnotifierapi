module.exports = (sequelize, DataTypes) => {
    let event = sequelize.define('event', {
        first_name: {
            type: DataTypes.STRING
        },
        last_name: {
            type: DataTypes.STRING
        },
        middle_name: {
            type: DataTypes.STRING
        },
        interval: {
            type: DataTypes.INTEGER
        },
        event_type: {
            type: DataTypes.INTEGER
        },
        description: {
            type: DataTypes.TEXT
        }
    }
      , {
      underscore: true,
      classMethods: {
        associate: (models) => {
          event.belongsTo(models.contractor);
          event.hasMany(models.order, {
            foreignKey: 'event_id'
          });
        }
      }
    }
    );

    return event;
};